package edu.uchicago.cs.quiz.pkuprys;


//QuizTracker follows the singleton pattern. Here's what you need to do to create a singleton
public class QuizTracker {
    //1. strictly encapsulate members by making them all private
    private int mCorrectAnswers = 0;
    private int mIncorrectAnswers = 0;
    private String name;
    private int mQuestionNum;
    //2. provide a static member of the same type of the enclosing class
    private  static QuizTracker sQuizTracker;

    //3. override its default constructor (remember that by default, Java provides a no-arg contractor)
    // and make it private. No one outside this class can instantiate it now.
    private QuizTracker(){}

    //4. provide a static getInstance() method. In this case, we don't instantiate until getInstance() is called for the first time (also know as lazy initialization)
    //All subsequent calls to getInstance() will return a reference to the already-instantiated sQuizTracker object. With this technique we can guarantee that
    //there is only ever one instance of this object, no matter how many times we call getInstance(). Because it is static, sQuizTracker it will persist in memory space throughout
    // the entire lifecycle of the app and accessible to any first-order component (e.g. Activity).

    //5. Activity members are often flushed -- for example in the event of a bounce.  Notice that QuizTracker is nowhere a member of any Activity.
    public static QuizTracker getInstance(){
        if (sQuizTracker == null){
            sQuizTracker = new QuizTracker();
            return sQuizTracker;
        }
        else {
            return sQuizTracker;
        }
    }

    public void setIncorrectAnswers(int incorrectAnswers) {
        mIncorrectAnswers = incorrectAnswers;
    }

    public void setCorrectAnswers(int correctAnswers) {
        mCorrectAnswers = correctAnswers;
    }

    public void reset(){
        setName("");
        setQuestionNum(1);
        setIncorrectAnswers(0);
        setCorrectAnswers(0);
    }

    public void again(){

        setQuestionNum(1);
        setIncorrectAnswers(0);
        setCorrectAnswers(0);
    }

    public int getQuestionNum() {
        return mQuestionNum;
    }

    public void setQuestionNum(int questionNum) {
        mQuestionNum = questionNum;
    }

    public String getName(){
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public void answeredWrong(){
        mIncorrectAnswers++;
    }

    public void answeredRight(){
        mCorrectAnswers++;
    }

    public void incrementQuestionNumber(){
        mQuestionNum++;
    }

    public int getCorrectAnswers(){
        return mCorrectAnswers;
    }

    public int getIncorrectAnswers(){
        return mIncorrectAnswers;
    }

    public int getTotalAnswers(){
        return mCorrectAnswers + mIncorrectAnswers;
    }


}
